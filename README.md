# FLCNetworking

[![CI Status](http://img.shields.io/travis/Konstantin.Portnov/FLCNetworking.svg?style=flat)](https://travis-ci.org/Konstantin.Portnov/FLCNetworking)
[![Version](https://img.shields.io/cocoapods/v/FLCNetworking.svg?style=flat)](http://cocoapods.org/pods/FLCNetworking)
[![License](https://img.shields.io/cocoapods/l/FLCNetworking.svg?style=flat)](http://cocoapods.org/pods/FLCNetworking)
[![Platform](https://img.shields.io/cocoapods/p/FLCNetworking.svg?style=flat)](http://cocoapods.org/pods/FLCNetworking)
[![Twitter](https://img.shields.io/badge/twitter-%40x0000ff-blue.svg?style=flat)](http://twitter.com/x0000ff)

## Usage

To run the example project, clone the repo, and run `pod install` from the `Example` directory first.

## Requirements

## Installation

FLCNetworking is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "FLCNetworking"
```

## Import 
```objective-c
#import <FLCNetworking/FLCNetworking.h>
```

## Author

[Konstantin.Portnov](https://twitter.com/x0000ff), [x0000ff@gmail.com](mailto:x0000ff@gmail.com)

## License

FLCNetworking is available under the MIT license. See the LICENSE file for more info.
