//
//  NSStringAdditionsTests.m
//  FLCNetworking
//
//  Created by x0000ff on 17/09/15.
//  Copyright (c) 2015 Konstantin.Portnov. All rights reserved.
//

//##############################################################################
#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "NSString+Additions.h"

//##############################################################################
@interface NSStringAdditionsTests : XCTestCase

@end

//##############################################################################
@implementation NSStringAdditionsTests

//##############################################################################
- (void)testStringWithLegalSymbolsIsNotPercentEscaped {
  
  NSString * input = @"SimpleString";
  NSString * outputExpected = @"SimpleString";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatReservedCharactersArePercentEscapedMinusQuestionMarkAndForwardSlash {
  
  NSString * input = @":#[]@!$&'()*+,;=";
  NSString * outputExpected = @"%3A%23%5B%5D%40%21%24%26%27%28%29%2A%2B%2C%3B%3D";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatReservedCharactersQuestionMarkAndForwardSlashAreNotPercentEscaped {
  
  NSString * input = @"?/";
  NSString * outputExpected = @"?/";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatUnreservedNumericCharactersAreNotPercentEscaped  {
  
  NSString * input = @"0123456789";
  NSString * outputExpected = @"0123456789";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatUnreservedLowercaseCharactersAreNotPercentEscaped  {
  
  NSString * input = @"abcdefghijklmnopqrstuvwxyz";
  NSString * outputExpected = @"abcdefghijklmnopqrstuvwxyz";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatUnreservedUppercaseCharactersAreNotPercentEscaped  {
  
  NSString * input = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  NSString * outputExpected = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatIllegalASCIICharactersArePercentEscaped  {
  
  NSString * input = @" \"#%<>[]\\^`{}|";
  NSString * outputExpected = @"%20%22%23%25%3C%3E%5B%5D%5C%5E%60%7B%7D%7C";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatStringWithAmpersandIsPercentEscaped  {
  
  NSString * input = @"foo&bar";
  NSString * outputExpected = @"foo%26bar";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatStringWithQuestionMarkIsNotPercentEscaped  {
  
  NSString * input = @"?foo?";
  NSString * outputExpected = @"?foo?";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatStringWithSlashIsNotPercentEscaped  {
  
  NSString * input = @"/bar/baz/qux";
  NSString * outputExpected = @"/bar/baz/qux";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatStringWithSpaceIsPercentEscaped  {
  
  NSString * input = @" foo bar ";
  NSString * outputExpected = @"%20foo%20bar%20";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatStringWithPlusIsPercentEscaped  {
  
  NSString * input = @"+foo+bar+";
  NSString * outputExpected = @"%2Bfoo%2Bbar%2B";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatStringWithPercentIsPercentEscaped  {
  
  NSString * input = @"percent%25";
  NSString * outputExpected = @"percent%2525";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
- (void)testThatStringWithNonLatinSymbolsIsPercentEscaped  {
  
  NSString * input = @"français日本語العربية😃русский";
  NSString * outputExpected = @"fran%C3%A7ais%E6%97%A5%E6%9C%AC%E8%AA%9E%D8%A7%D9%84%D8%B9%D8%B1%D8%A8%D9%8A%D8%A9%F0%9F%98%83%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B9";
  NSString * outputActual = [input escape];
  
  XCTAssertTrue([outputActual isEqualToString:outputExpected], @"\n\nEscaped incorrectly.\nExpected: \"%@\"\nActual:   \"%@\"\n\n", outputExpected, outputActual);
}

//##############################################################################
@end
//##############################################################################
