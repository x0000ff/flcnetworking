//
//  FLCAppDelegate.h
//  FLCNetworking
//
//  Created by Konstantin.Portnov on 09/17/2015.
//  Copyright (c) 2015 Konstantin.Portnov. All rights reserved.
//

@import UIKit;

@interface FLCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
