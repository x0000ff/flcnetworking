//
//  main.m
//  FLCNetworking
//
//  Created by Konstantin.Portnov on 09/17/2015.
//  Copyright (c) 2015 Konstantin.Portnov. All rights reserved.
//

@import UIKit;
#import "FLCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FLCAppDelegate class]));
    }
}
