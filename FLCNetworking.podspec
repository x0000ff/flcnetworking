#
# Be sure to run `pod lib lint FLCNetworking.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "FLCNetworking"
  s.version          = "0.0.2"
  s.summary          = "My network bicycle ;)"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
My network bicycle ;) I wrote this library to evoid some boilerplate code.
                       DESC

  s.homepage         = "https://bitbucket.org/x0000ff/flcnetworking"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Konstantin.Portnov" => "x0000ff@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/x0000ff/FLCNetworking.git", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/x0000ff'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'FLCNetworking' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.public_header_files = 'Pod/Classes/**/*.h'

  # s.frameworks = 'UIKit', 'MapKit'
  s.frameworks = 'UIKit'

  # s.dependency 'AFNetworking', '~> 2.3'
end
