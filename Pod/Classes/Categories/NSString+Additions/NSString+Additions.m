//
//  NSString+Additions.m
//  FLCNetworking
//
//  Created by x0000ff on 15/09/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "NSString+Additions.h"

//##############################################################################
@implementation NSString (Additions)

//##############################################################################
- (NSString *) escape {
  
  
  CFStringRef urlString = CFURLCreateStringByAddingPercentEscapes(
                                                                  NULL,
                                                                  (CFStringRef)self,
                                                                  NULL,
                                                                  (CFStringRef)@":#[]@!$&'()*+,;=", // does not include "?" or "/" due to RFC 3986 - Section 3.4
                                                                  kCFStringEncodingUTF8 );
  return (NSString *)CFBridgingRelease(urlString);
}

//##############################################################################
@end
//##############################################################################
